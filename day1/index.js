const {input} = require('./input')

const items = input.split('\n\n')


// part 1
let highest = 0;

items.forEach(item => {
    const total = item.split('\n').reduce((acc, current) => (+current) + acc, 0)
    if(total > highest) {
        highest = total;
    }
})

console.log('Part 1:', highest)

// part 2

const highestThree = [0, 0, 0]

items.forEach(item => {
    const total = item.split('\n').reduce((acc, current) => (+current) + acc, 0)
    if(highestThree.findIndex(c => c < total) !== -1) {
        highestThree[0] = total
        highestThree.sort();
    }
})

console.log('Part 2:', highestThree.reduce((acc, current) => acc + current, 0))
