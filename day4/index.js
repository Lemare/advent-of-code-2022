const {input} = require('./input')

const data = input.split('\n');

const allPairs = data.map(d => d.split(',').map(a => {
    const values = []
    const [val1, val2] = a.split('-')
    for(let i = +val1; i <= +val2; i++) {
        values.push(i)
    }
    return values
}))


// Part 1

const matching = allPairs.filter(([val1, val2]) => {
    if(val1.every(c => val2.includes(c))) {
        return true;
    }
    return val2.every(c => val1.includes(c));

})

console.log('Part 1:', matching.length)

// Path 2

const anyMatching = allPairs.filter(([val1, val2]) => {
    if(val1.find(c => val2.includes(c))) {
        return true;
    }
    return val2.find(c => val1.includes(c));

})

console.log('Part 2:', anyMatching.length)