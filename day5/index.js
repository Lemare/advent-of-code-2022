const {input} = require('./input')

const allSteps = input.split('\n');

// Part 1

const startingPosition1 = [
    ['C', 'Z', 'N', 'B', 'M', 'W', 'Q', 'V'],
    ['H', 'Z', 'R', 'W', 'C', 'B'],
    ['F', 'Q', 'R', 'J'],
    ['Z', 'S', 'W', 'H', 'F', 'N', 'M', 'T'],
    ['G', 'F', 'W', 'L', 'N', 'Q', 'P'],
    ['L', 'P', 'W'],
    ['V', 'B', 'D', 'R', 'G', 'C', 'Q', 'J'],
    ['Z', 'Q', 'N', 'B', 'W'],
    ['H', 'L', 'F', 'C', 'G', 'T', 'J']
]

allSteps.forEach(step => {
    const amount = step.split('from')[0].split('move')[1].trim()
    const from = step.split('from')[1].split('to')[0].trim()
    const to = step.split('to')[1].trim()

    for(let i = 0; i < amount; i++) {
        const value = startingPosition1[from-1].pop()
        startingPosition1[to-1].push(value)
    }
})

console.log('Part 1:', startingPosition1.map(c => c[c.length - 1]).join(''))

// Part 2

const startingPosition2 = [
    ['C', 'Z', 'N', 'B', 'M', 'W', 'Q', 'V'],
    ['H', 'Z', 'R', 'W', 'C', 'B'],
    ['F', 'Q', 'R', 'J'],
    ['Z', 'S', 'W', 'H', 'F', 'N', 'M', 'T'],
    ['G', 'F', 'W', 'L', 'N', 'Q', 'P'],
    ['L', 'P', 'W'],
    ['V', 'B', 'D', 'R', 'G', 'C', 'Q', 'J'],
    ['Z', 'Q', 'N', 'B', 'W'],
    ['H', 'L', 'F', 'C', 'G', 'T', 'J']
]

allSteps.forEach(step => {
    const amount = step.split('from')[0].split('move')[1].trim()
    const from = step.split('from')[1].split('to')[0].trim()
    const to = step.split('to')[1].trim()

    const moves = []
    for(let i = 0; i < amount; i++) {
        moves.push(startingPosition2[from-1].pop())
    }
    startingPosition2[to-1].push(...moves.reverse())
})

console.log('Part 2:', startingPosition2.map(c => c[c.length - 1]).join(''))
