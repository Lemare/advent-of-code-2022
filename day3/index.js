const {input} = require('./input')

const priorities = {
    a: 1,
    b: 2,
    c: 3,
    d: 4,
    e: 5,
    f: 6,
    g: 7,
    h: 8,
    i: 9,
    j: 10,
    k: 11,
    l: 12,
    m: 13,
    n: 14,
    o: 15,
    p: 16,
    q: 17,
    r: 18,
    s: 19,
    t: 20,
    u: 21,
    v: 22,
    w: 23,
    x: 24,
    y: 25,
    z: 26
}

const getPriority = (letter) => {
    if (Object.keys(priorities).includes(letter)) {
        return priorities[letter]
    } else {
        return priorities[letter.toLowerCase()] + 26
    }
}

// Part 1

const allRucksacks = input.split('\n');

const allItems = []
allRucksacks.forEach((currentValue) => {
    const halfSize = currentValue.length / 2;
    const [compartment1, compartment2] = [currentValue.substring(0, Math.floor(halfSize)), currentValue.substring(Math.ceil(currentValue.length / 2))]
    for (let i = 0; i < compartment1.length; i++) {
        const character = compartment1.charAt(i);
        if (compartment2.includes(character)) {
            allItems.push(character)
            break;
        }
    }
})

const total = allItems.reduce((acc, currentValue) => getPriority(currentValue) + acc, 0)
console.log('Part 1:', total)

// Part 2
const groups = [];
let newGroup = []
allRucksacks.forEach((rucksack, index) => {
    if (index % 3 === 0 && newGroup.length > 0) {
        groups.push(newGroup)
        newGroup = []
    }
    newGroup.push(rucksack)
})
groups.push(newGroup)

const commonItemsInGroups = []

groups.forEach((group) => {
    for (let i = 0; i < group[0].length; i++) {
        const character = group[0].charAt(i);
        if (group[1].includes(character) && group[2].includes(character)) {
            commonItemsInGroups.push(character)
            break;
        }
    }
})

const total2 = commonItemsInGroups.reduce((acc, currentValue) => getPriority(currentValue) + acc, 0)
console.log('Part 2:', total2)