const {input} = require('./input')

const allCombinations = input.split('\n')

const pointMap = {
    Rock: 1,
    Paper: 2,
    Scissors: 3,
}

const keyMap = {
    X: 'Rock',
    A: 'Rock',
    Y: 'Paper',
    B: 'Paper',
    Z: 'Scissors',
    C: 'Scissors',
}

// Part 1

const map = {
    [keyMap.A + keyMap.X]: 3,
    [keyMap.A + keyMap.Y]: 6,
    [keyMap.A + keyMap.Z]: 0,
    [keyMap.B + keyMap.X]: 0,
    [keyMap.B + keyMap.Y]: 3,
    [keyMap.B + keyMap.Z]: 6,
    [keyMap.C + keyMap.X]: 6,
    [keyMap.C + keyMap.Y]: 0,
    [keyMap.C + keyMap.Z]: 3,
}

const total1 = allCombinations.reduce((acc, currentValue) => {
    const [a,b] = currentValue.split(' ')
    const key = keyMap[a] + keyMap[b]
    return acc + map[key] + pointMap[keyMap[b]]
}, 0);

console.log('Part 1:', total1)

// Part 2

const winMap = {
    A: 'C',
    B: 'A',
    C: 'B',
}

const loseMap = {
    A: 'B',
    B: 'C',
    C: 'A',
}

const total2 = allCombinations.reduce((acc, currentValue) => {
    const [a,b] = currentValue.split(' ')
    if(b === 'X') {
        return acc + pointMap[keyMap[winMap[a]]];
    } else if(b === 'Y') {
        return acc + 3 + pointMap[keyMap[a]];
    } else {
        return acc + 6 + pointMap[keyMap[loseMap[a]]];
    }
}, 0);

console.log('Part 2:', total2)